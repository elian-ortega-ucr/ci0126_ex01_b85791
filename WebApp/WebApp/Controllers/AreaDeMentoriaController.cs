﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class AreaDeMentoriaController : Controller
    {
        private Datos_ExamenI_B85791Entities db = new Datos_ExamenI_B85791Entities();

        // GET: AreaDeMentoria
        public ActionResult Index()
        {
            return View(db.AreaDeMentorias.ToList());
        }

        // GET: AreaDeMentoria/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaDeMentoria areaDeMentoria = db.AreaDeMentorias.Find(id);
            if (areaDeMentoria == null)
            {
                return HttpNotFound();
            }
            return View(areaDeMentoria);
        }

        // GET: AreaDeMentoria/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: AreaDeMentoria/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Nombre,Descripcion")] AreaDeMentoria areaDeMentoria)
        {
            if (ModelState.IsValid)
            {
                db.AreaDeMentorias.Add(areaDeMentoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(areaDeMentoria);
        }

        // GET: AreaDeMentoria/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaDeMentoria areaDeMentoria = db.AreaDeMentorias.Find(id);
            if (areaDeMentoria == null)
            {
                return HttpNotFound();
            }
            return View(areaDeMentoria);
        }

        // POST: AreaDeMentoria/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Nombre,Descripcion")] AreaDeMentoria areaDeMentoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(areaDeMentoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(areaDeMentoria);
        }

        // GET: AreaDeMentoria/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AreaDeMentoria areaDeMentoria = db.AreaDeMentorias.Find(id);
            if (areaDeMentoria == null)
            {
                return HttpNotFound();
            }
            return View(areaDeMentoria);
        }

        // POST: AreaDeMentoria/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AreaDeMentoria areaDeMentoria = db.AreaDeMentorias.Find(id);
            db.AreaDeMentorias.Remove(areaDeMentoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
