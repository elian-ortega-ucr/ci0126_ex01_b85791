﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class MentoriaController : Controller
    {
        private Datos_ExamenI_B85791Entities db = new Datos_ExamenI_B85791Entities();

        // GET: Mentoria
        public ActionResult Index()
        {
            var mentorias = db.Mentorias.Include(m => m.Docente).Include(m => m.Estudiante);
            return View(mentorias.ToList());
        }

        // GET: Mentoria/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            return View(mentoria);
        }

        // GET: Mentoria/Create
        public ActionResult Create()
        {
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre");
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Nombre");
            return View();
        }

        // POST: Mentoria/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdMentoria,CedulaDocente,CedulaEstudiante")] Mentoria mentoria)
        {
            if (ModelState.IsValid)
            {
                db.Mentorias.Add(mentoria);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // GET: Mentoria/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Nombre", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // POST: Mentoria/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdMentoria,CedulaDocente,CedulaEstudiante")] Mentoria mentoria)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mentoria).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", mentoria.CedulaDocente);
            ViewBag.CedulaEstudiante = new SelectList(db.Estudiantes, "Cedula", "Carne", mentoria.CedulaEstudiante);
            return View(mentoria);
        }

        // GET: Mentoria/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mentoria mentoria = db.Mentorias.Find(id);
            if (mentoria == null)
            {
                return HttpNotFound();
            }
            return View(mentoria);
        }

        // POST: Mentoria/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Mentoria mentoria = db.Mentorias.Find(id);
            db.Mentorias.Remove(mentoria);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
