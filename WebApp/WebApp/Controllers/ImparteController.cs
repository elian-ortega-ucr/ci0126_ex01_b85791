﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ImparteController : Controller
    {
        private Datos_ExamenI_B85791Entities db = new Datos_ExamenI_B85791Entities();

        // GET: Imparte
        public ActionResult Index()
        {
            var impartes = db.Impartes.Include(i => i.AreaDeMentoria).Include(i => i.Docente);
            return View(impartes.ToList());
        }

        // GET: Imparte/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imparte imparte = db.Impartes.Find(id);
            if (imparte == null)
            {
                return HttpNotFound();
            }
            return View(imparte);
        }

        // GET: Imparte/Create
        public ActionResult Create()
        {
            ViewBag.IdAreaMentoria = new SelectList(db.AreaDeMentorias, "Id", "Nombre");
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre");
            return View();
        }

        // POST: Imparte/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "IdImparte,CedulaDocente,IdAreaMentoria")] Imparte imparte)
        {
            if (ModelState.IsValid)
            {
                db.Impartes.Add(imparte);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdAreaMentoria = new SelectList(db.AreaDeMentorias, "Id", "Nombre", imparte.IdAreaMentoria);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", imparte.CedulaDocente);
            return View(imparte);
        }

        // GET: Imparte/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imparte imparte = db.Impartes.Find(id);
            if (imparte == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdAreaMentoria = new SelectList(db.AreaDeMentorias, "Id", "Nombre", imparte.IdAreaMentoria);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", imparte.CedulaDocente);
            return View(imparte);
        }

        // POST: Imparte/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "IdImparte,CedulaDocente,IdAreaMentoria")] Imparte imparte)
        {
            if (ModelState.IsValid)
            {
                db.Entry(imparte).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdAreaMentoria = new SelectList(db.AreaDeMentorias, "Id", "Nombre", imparte.IdAreaMentoria);
            ViewBag.CedulaDocente = new SelectList(db.Docentes, "Cedula", "Nombre", imparte.CedulaDocente);
            return View(imparte);
        }

        // GET: Imparte/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Imparte imparte = db.Impartes.Find(id);
            if (imparte == null)
            {
                return HttpNotFound();
            }
            return View(imparte);
        }

        // POST: Imparte/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Imparte imparte = db.Impartes.Find(id);
            db.Impartes.Remove(imparte);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
