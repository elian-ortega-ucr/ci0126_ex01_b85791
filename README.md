# Parte 2 Examen I - B85791
## Logros
Se logro desarrollar una aplicación web que cuenta con una conexión a la bases de datos donde se encuentra la información de los docentes, estudiantes, áreas de mentor y la relaciones entre estas. En la aplicación web se pueden visualizar las listas de los pertenecientes a cada uno de estas tablas y agregar o realizar modificaciones a las mismas. 

Si existiese la posibilidad de posibles iteraciones me gustaría mejorar el diseño de la aplicación y realizar pruebas de los diferentes módulos existentes. 
## Cumplimiento de los principios solid:
### Single Responsability Principle
Este principio nos dice que cada modulo o clase de nuestro proyecto debe de tener una sola responsabilidad y funcionalidad del software. Una manera de facilitar el cumplimiento de este principio es la separación de archivos en múltiples carpetas principalmente en Lógica de Negocios/ Controladores, UI/Views, Interfaces, Servicios, etc. Con esta separación se nos facilitara la separación de los módulos mas grandes de desarrollo. Sin embargo hay que recordad que este principio aplica al código por lo que cada uno de nuestros metodos y clases debe de cumplir una única tarea. 

### Open/Close Principle
El objetivo principal de este principio es la posibilidad de que una entidad de software pueda extenderese pero no modificarse, de esta manera la funcionalidad base esta protegida. Una recomendación para cumplir este principio seria un análisis previo de las diferentes funcionalidades de nuestro sistema donde se puedan encontrar funcionalidades comunes y de esta manera extraer estas a módulos / clases / metodos principales que permitan una reutilización de código, fácil mantenimiento y esto sin poner en riesgo el código fuente de estas funcionalidades. 

### Liskov Substitution Principle
Este principio esta directamente relacionado con el uso de interfaces en el código. Por lo que la recomendación es utilizar interfaces de clases con cada servicio o calses similares de esta manera en un momento que se quiera hacer el cambio de un servicio o clase por una implementación diferente lo podremos hacer solo creando una nueva clase o servicio que cumpla con los requisitos de la interfaz. Esto ayudara a la rutilizacion de código, reducirá errores y permitirá una comunicación sencilla en caso que haya diferentes equipos trabajando en un desarrollo que necesita comunicarse entre equipos.

### Interface Segregation Principle
Con una fuerte relación a la recomendación hecha previamente este principio nos dice que no debemos de agregar metodos no comunes para todos las subclases en la interfaz. Con esto me refiero a que una clase que hereda no debe de ser expuesta a metodos que no utilizara. Por esto, a recomendación es ser muy cuidadoso al crear nuestras interfaces solo con metodos comunes, en el caso que solo algunos de nuestros clases utiliza un método este podría ser extraido en otra interfaz permitiendo que solo lo que la vayan a utilizar la implementen o la modifiquen de acuerdo a sus necesidades.

### Dependency Inversion Principle
Este principio nos insta a reducir la dependencia entre módulos e implementaciones de clases en nuestro desarrollo. Esto lo podríamos lograr utilizando abstracciones de objetos y no sus instancias, ya que de esta manera reducimos una dependencia directa de una implementación especifica si no que podríamos utilizar cualquier otra clase que herede de esta clase abstracta. Esto mas adelante podría ser mejorado utilizando infección de dependencias en el código. 




## Retrospectiva
## ¿Cuáles son sus impresiones generales del curso? ¿Qué ha contribuido en su aprendizaje y qué no lo ha hecho? ¿Qué mantendría igual? ¿Qué recomendaciones de mejora realizaría? Justifique.
Hasta el momento el curso me ha gustado mucho, dentro de las cosas que destaco es el uso de herramientas para estrategia git y ágiles muy usado en el ambiente laboral. Sin embargo, me gustaría que se utilizaran tecnologías que se mueven mas en el mercado actual no específicamente una pero de las mas demandas en la actualidad. 

## ¿Considera que las actividades realizadas durante las clases le ayudaron en su proceso de aprendizaje de los conceptos y habilidades desarrolladas en el curso? Justifique a partir de las distintas actividades realizadas durante las clases.
Siento que el marco principal del curso es la parte de estrategia Scrum y desarrollo ágil, y la practica real en un proyecto siento que es excelente porque me permitió realizar una relación directa entre teoría y practica.

## ¿Qué recomendaciones de mejora realizaría específicamente desde la perspectiva de que el curso se realiza de manera virtualizada?
Sesiones de livecoding al usar las herramientas, y demostraciones de diferentes cosas del mundo profesional a pesar que stas sean superficiales pero permitir a los estudiantes conocer diferentes herramientas o datos importantes.

## Cómo ha aportado para el éxito en el proyecto como desarrollador/a, cómo miembro del equipo y cómo miembro de los equipos? ¿Qué acciones puntuales puede aplicar para el éxito de las siguientes iteraciones
Siento que al haber trabajado en equipo previamente he aportado en la estrategia git y en el manejo de las ramas de mi equipo tambien en cuestioines de manejo de estados y algunos detalles técnicos de programaciones n general no necesariamente de la herramienta . Siento que podría coordinar me mejor con mis compa;eros para dividir las tareas. 