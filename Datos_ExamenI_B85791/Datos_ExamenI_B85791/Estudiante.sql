﻿CREATE TABLE [dbo].[Estudiante]
(
	[Cedula] INT IDENTITY(1,1) NOT NULL,
    [Carne] NVARCHAR(20) NOT NULL, 
    [Nombre] NVARCHAR(50) NULL, 
    [Carrera] NVARCHAR(30) NULL, 
    [Enfasis] NVARCHAR(50) NULL
    PRIMARY KEY CLUSTERED ([Cedula] ASC)
)
