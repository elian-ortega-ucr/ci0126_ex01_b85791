﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


SET IDENTITY_INSERT AreaDeMentoria ON;
INSERT INTO AreaDeMentoria (Id, Nombre,Descripcion)
VALUES
(1, 'Career path', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed b.'), 
(2, 'Areas de interés profesional', 'Fusce bibendum sed eros eu feugiat. Maecenas.'),
(3, 'actividades extracurriculares', 'Suspendisse congue vestibulum risus, ac pha.'),
(4, 'Grupos de investigación', 'Nullam venenatis id nunc quis pretium. Aenean vit');
SET IDENTITY_INSERT AreaDeMentoria OFF;


SET IDENTITY_INSERT Estudiante ON;
Insert INTO Estudiante (Cedula, Carne, Nombre, Carrera, Enfasis)
VALUES
(201,'B1', 'Estudiante Maria', 'Computacion', 'IS'),
(202, 'B2','Estudiante Juan', 'Computacion', 'TI'),
(203,'B3', 'Estudiante Luisa', 'Computacion', 'CC'),
(204,'B4', 'Estudiante Pedro', 'Computacion', 'IS');
SET IDENTITY_INSERT Estudiante OFF;


SET IDENTITY_INSERT Docente ON;
INSERT INTO Docente (Cedula, Nombre, Grado, Carrera)
VALUES 
(101, 'Maria', 'PostGrado','Computacion'),
(102, 'Juan', 'PostGrado','Computacion'),
(103, 'Luisa', 'PostGrado','Computacion'),
(104, 'Pedro', 'PostGrado','Computacion');
SET IDENTITY_INSERT Docente OFF;


SET IDENTITY_INSERT Mentoria ON;
INSERT INTO Mentoria  (IdMentoria, CedulaDocente, CedulaEstudiante)
VALUES
(1,101,201),
(2,102,202),
(3,103,203),
(4,104,204),

(5,101,202);
SET IDENTITY_INSERT Mentoria OFF;


SET IDENTITY_INSERT Imparte ON;
INSERT INTO Imparte (IdImparte, CedulaDocente, IdAreaMentoria)
VALUES
(1,101,1),
(2,102,2),
(3,103,3),
(4,104,4);
SET IDENTITY_INSERT Imparte OFF;


