﻿CREATE TABLE [dbo].[Imparte]
(
	[IdImparte] INT IDENTITY (1,1) NOT NULL, 
    [CedulaDocente] INT NOT NULL, 
    [IdAreaMentoria] INT NOT NULL
    PRIMARY KEY CLUSTERED ([IdImparte] ASC), 
    CONSTRAINT [FK_Imparte_AreaMentoria] FOREIGN KEY ([IdAreaMentoria]) REFERENCES [AreaDeMentoria]([Id]) ON DELETE CASCADE,
    CONSTRAINT [FK_Imparte_Docente] FOREIGN KEY ([CedulaDocente]) REFERENCES [Docente]([Cedula]) ON DELETE CASCADE
)
