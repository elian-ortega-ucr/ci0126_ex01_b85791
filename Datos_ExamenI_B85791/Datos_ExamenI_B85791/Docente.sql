﻿CREATE TABLE [dbo].[Docente]
(
	[Cedula] INT IDENTITY(1,1) NOT NULL, 
    [Nombre] NVARCHAR(50) NULL, 
    [Grado] NVARCHAR(20) NULL, 
    [Carrera] NVARCHAR(30) NULL
    PRIMARY KEY CLUSTERED ([Cedula] ASC)
)
