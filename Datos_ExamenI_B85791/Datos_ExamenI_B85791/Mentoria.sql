﻿CREATE TABLE [dbo].[Mentoria]
(
	[IdMentoria] INT IDENTITY (1,1) NOT NULL, 
    [CedulaDocente] INT NOT NULL, 
    [CedulaEstudiante] INT NOT NULL
    PRIMARY KEY CLUSTERED ([IdMentoria] ASC), 
    CONSTRAINT [FK_Mentoria_Estudiante] FOREIGN KEY ([CedulaEstudiante]) REFERENCES [Estudiante]([Cedula]) ON DELETE CASCADE,
    CONSTRAINT [FK_Mentoria_Docente] FOREIGN KEY ([CedulaDocente]) REFERENCES [Docente]([Cedula]) ON DELETE CASCADE
)
