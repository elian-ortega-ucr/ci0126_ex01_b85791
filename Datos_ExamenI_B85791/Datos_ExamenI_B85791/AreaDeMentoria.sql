﻿CREATE TABLE [dbo].[AreaDeMentoria]
(
	[Id] INT IDENTITY(1,1) NOT NULL, 
    [Nombre] NVARCHAR(30) NULL, 
    [Descripcion] NVARCHAR(300) NULL
    PRIMARY KEY CLUSTERED ([Id] ASC)
)
